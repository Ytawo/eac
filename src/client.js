var net = require('net');
var ws = require('ws'), WebSocket = ws;

var specialFunctions = [ "OnConnect", "OnClose" ];

/*
    Connect to a server.
        address: socket path on which it must connect. Depending on the protocol, the format must be the following:
            WebSocket: 'ws://<addr>:<port>'
            tcp: '<addr>:<port>'
            unix: '/path/to/socket'
        eventMap: map of event name -> function
        callback: optional. Function to execute after it is connected.
        debugMode: optional. If true, information sent and received are displayed. Default:false.
*/
exports.ConnectAPI = function (address, eventMap, callback, debugMode) {
    /* Set the debug variable. */
    var debug = debugMode === true ? true : false;

    /* If callback isn't specified, assign it to an empty function. */
    if (callback === undefined)
        callback = function () {};

    var protocol;

    if (/^ws:\/\//.test(address)) protocol = "ws";
    else if (/:\d+/.test(address)) protocol = "tcp";
    else protocol = "unix";

    return Connect(protocol, address, eventMap, callback, debug);
}

function EacClient(socket, events, protocol, debug) {
    this.__vars = new Object();
    this.__vars.socket = socket;
    this.__vars.protocol = protocol;
    this.__vars.nextCallId = 1;
    this.__vars.callbacks = new Object();
    this.__vars.events = events;
    this.__vars.connected = false;
    this.__vars.ready = false;
    this.__vars.debug = debug;
}

EacClient.prototype.Disconnect = function (callback) {
    if (this.__vars.protocol == "ws")
        this.__vars.socket.close();
    else
        this.__vars.socket.end();

    if (typeof callback === "function") callback();
}

function Connect(protocol, address, eventMap, callback, debug, client, nbTry) {
    if (nbTry == undefined)
        nbTry = 1;

    /* If we already tried 10 times, return null. */
    if (nbTry > 10) {
        console.error("Address " + address + " doesn't seem to be reachable.");
        return null;
    }

    /* Connect to the server. */
    var socket;
    if (protocol == "ws") {
        socket = new WebSocket(address);
    } else if (protocol == "tcp") {
        var port = address.substring(address.lastIndexOf(':') + 1, address.length);
        var host = address.substring(0, address.lastIndexOf(':'));
        socket = net.connect(port, host);
    } else {
        socket = net.connect(address);
    }

    /* Setup the EAC client. */
    if (typeof client != "object") {
        var events = SetupEvents(eventMap);
        client = new EacClient(socket, events, protocol, debug);
    } else {
        client.__vars.socket = socket;
    }

    socket.on('connect', function() { client.__vars.connected = true; });

    socket.on('error', function (e) {
        /* If it was already connected to the module, display the error. */
        if (client.__vars.connected == true) {
            console.error("Error: disconnected from '" + address + "'.");
            console.error(e);
        } else { /* Else, the module was not ready when we tried to connect, so we retry 100 ms later. */
            if (debug) console.log("Unable to connect to '" + address + "'. Retrying in 100ms.");
            setTimeout(function () { Connect(protocol, address, eventMap, callback, debug, client, nbTry + 1); }, 100);
        }
    });

    socket.on(protocol == 'ws' ? 'message' : 'data', function (bigBuffer) {
        var subBuffers = bigBuffer.toString().split("\n");
        subBuffers.forEach(function(buffer) {
            if (buffer == "") return;
            if (client.__vars.debug) console.log("RCV: " + buffer);
            var obj = JSON.parse(buffer);

            if (!client.__vars.ready) {
                SetupAPI(client, obj);
                client.__vars.ready = true;
                callback();
            } else if (obj.evt !== undefined) {
                var f = client.__vars.events[obj.evt];
                if (f !== undefined) {
                    f.apply(f, obj.args);
                }
            } else {
                client.__vars.callbacks[obj.id](obj.data);
                delete client.__vars.callbacks[obj.id];
            }
        });
    });

    return client;
}

function SetupEvents(eventMap) {
    var events = new Object();
    if (eventMap instanceof Array) {
        eventMap.forEach(function (f) {
            if (typeof f === "function") {
                events[f.name] = f;
            }
        });
    } else if (eventMap instanceof Object) {
        for (var n in eventMap) {
            var f = eventMap[n];
            if (typeof f === "function") {
                events[n] = f;
            }
        }
    }

    return events;
}

function SetupAPI(client, functionList) {
    functionList.forEach(function (name) {
        client[name] = function () {
            var lastIndex = Object.keys(arguments).length - 1;
            var lastArg = arguments[lastIndex];
            var callId, packet;
            if (typeof lastArg === "function") { // This is the callback
                client.__vars.callbacks[client.__vars.nextCallId] = lastArg;
                delete arguments[lastIndex];
                callId = client.__vars.nextCallId;
                client.__vars.nextCallId += 1;
            } else {                             // No callback was specified
                callId = null;
            }
            var args = new Array();
            for (var k in arguments) args.push(arguments[k]);

            var packet = JSON.stringify({id:callId, name:name, args:args});
            if (client.__vars.debug) console.log("SNT: " + packet);

            if (client.__vars.protocol == "ws") {
                client.__vars.socket.send(packet + "\n");
            } else {
                client.__vars.socket.write(packet + "\n");
            }
        };
    });
}
