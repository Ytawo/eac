var net = require('net');
var ws = require('ws');
var fs = require('fs');

var specialFunctions = [ "OnConnect", "OnClose" ];

/*
    Create an API server.
        api: array of functions that are part of the API
        eventList: array of string that are the function that the server can call
        protocol: 'unix', 'tcp', 'ws'
        socketFile: socket on which the server must listen
        callback: Optional. Function to execute after it is started.
        debugMode: optional. If true, information sent and received are displayed. Default:false.
*/
exports.CreateAPI = function (api, eventList, protocol, fileOrPort, callback, debugMode) {
    /* Check that the specified protocol is valid. */
    if (['unix', 'tcp', 'ws'].indexOf(protocol) == -1) {
        console.error("Invalid protocol.");
        return;
    }

    /* If callback isn't specified or is null, assign it to an empty function. */
    if (callback == null)
        callback = function () {};

    /* Set the debug variable. */
    var debug = debugMode === true ? true : false;

    /* Setup the functions. */
    var functions = SetupApi(api, debug);

    /* Clear the socket if it already exists. */
    if (protocol == "unix" && fs.existsSync(fileOrPort))
        fs.unlinkSync(fileOrPort);

    /* Create the actual unix, tcp or ws server. */
    var tcpServer;
    if (protocol == "ws") {
        tcpServer = new ws.Server({port:fileOrPort}, callback());
    } else {
        tcpServer = net.createServer();
        tcpServer.listen(fileOrPort, function () { callback(); });
    }

    /* Create the EAC server. */
    var server = new EacServer(functions, protocol, tcpServer, debug);

    /* Setup the server. */
    SetupServer(server, eventList);



    return server;
}

function EacServer(functions, protocol, tcpServer, debug) {
    this.__vars = new Object();
    this.__vars.protocol = protocol;
    this.__vars.functions = functions;
    this.__vars.tcpServer = tcpServer;
    this.__vars.debug = debug;
    this.__vars.counter = 1;
    this.clients = new Object();
}

function Client(socket, id) {
    this.__vars = new Object();
    this.__vars.socket = socket;
    this.__vars.id = id;
}

function SetupServer(server, eventList) {
    var clients = server.clients;

    /* Setup the events sending. */
    eventList.forEach(function (e) {
        if (typeof e === "string") {
            server.clients[e] = function () {
                var args = new Array();
                for (var a in arguments) args.push(arguments[a]);
                for (var c in clients) {
                    if (/\d+/.test(c)) {
                        SendEvent(clients[c].__vars.socket, server.__vars.protocol, e, args, server.__vars.debug);
                    }
                }
            };
        }
    });

    server.__vars.tcpServer.on('connection', function (sock) {
        var id = server.__vars.counter++;
        var client = new Client(sock, id);
        server.clients[id] = client;
        SetupClientSocket(client, server, eventList);
        SendAPI(sock, server.__vars.protocol, server.__vars.functions, server.__vars.__debug);
        if (server.__vars.debug) { console.log("Client " + client.__vars.id + " has just connected."); }

        var onConnection = server.__vars.functions.OnConnect;
        if (onConnection !== undefined)
            onConnection(id);
    });
}

function SetupClientSocket(client, server, eventList) {
    var sock = client.__vars.socket;
    var protocol = server.__vars.protocol;
    var debug = server.__vars.debug;
    var id = client.__vars.id;

    eventList.forEach(function (e) {
        if (typeof e === "string") {
            client[e] = function () {
                var args = new Array();
                for (var a in arguments) args.push(arguments[a]);
                SendEvent(sock, protocol, e, args, debug);
            };
        }
    });

    sock.on('close', function () {
        var onClose = server.__vars.functions.OnClose;
        if (onClose !== undefined)
            onClose(id);

        if (server.__vars.debug) { console.log("Client " + client.__vars.id + " disconnected."); }
        delete server.clients[client.__vars.id];
    });

    sock.on(protocol == 'ws' ? 'message' : 'data', function (bigBuffer) {
        var subBuffers = bigBuffer.toString().split("\n");
        subBuffers.forEach(function (buffer) {
            if (buffer == "") return;
            if (debug) console.log("RCV: " + buffer);

            var call;
            try {
                call = JSON.parse(buffer);
            } catch (e) {
                if (debug) console.log(e);
                SendResponse(sock, protocol, null, "InvalidJSON", debug);
                return;
            }

            var func = server.__vars.functions[call.name];

            if (typeof func != "function") {
                SendResponse(sock, protocol, call.id, "UndefinedFunction", debug);
            } else if (specialFunctions.indexOf(call.name) == -1 ) {
                func.apply(new FunctionCaller(id, sock, protocol, call.id, debug), call.args);
            }
        });
    });
}

function SetupApi(api, debug) {
    var functions = new Object();
    if (api instanceof Array) {
        api.forEach(function (f) {
            if (typeof f === "function") {
                functions[f.name] = f;
            }
        });
    } else if (api instanceof Object) {
        for (var name in api) {
            var f = api[name];
            if (typeof f === "function") {
                functions[name] = f;
            }
        }
    }

    return functions;
}

function FunctionCaller(id, sock, protocol, callId, debug) {
    this.id = id;
    this.sock = sock;
    this.protocol = protocol;
    this.callId = callId;
    this.debug = debug;
}

FunctionCaller.prototype.return = function (data) {
    if (this.callId != null) {
        SendResponse(this.sock, this.protocol, this.callId, data, this.debug);
    }
}

function SendAPI(socket, protocol, functions, debug) {
    var funcNames = JSON.stringify(Object.keys(functions));
    Send(socket, protocol, funcNames, debug);
}

function SendResponse(socket, protocol, callId, data, debug) {
    var response = JSON.stringify({id:callId, data:data});
    Send(socket, protocol, response, debug);
}

function SendEvent(socket, protocol, e, args, debug) {
    var evt = JSON.stringify({evt:e, args:args});
    Send(socket, protocol, evt, debug);
}

function Send(socket, protocol, packet, debug) {
    if (debug) console.log("SNT " + packet);
    if (protocol == "ws")
        socket.send(packet + "\n");
    else
        socket.write(packet + "\n");
}

EacServer.prototype.Stop = function (callback) {
    this.__vars.tcpServer.close();
    this.__vars.tcpServer = null;
    if (callback !== undefined)
        callback();
}
