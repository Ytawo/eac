/*
    EAC = Easy API Creator
*/

exports.CreateAPI = require('./server.js').CreateAPI;

exports.ConnectAPI = require('./client.js').ConnectAPI;
