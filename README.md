# Easy API Creator

Easily provide your functions to remote clients:

* Create the API server;
* Connected clients can use the functions provided by the server without caring about network;
* The server can fire events to its clients without caring about network.

An example is provided at the end.

EAC currently supports 3 protocols:

* **Unix sockets**: use it if the server will be accessed only on the same machine (e.g. inside of a 'multi-components' program);
* **TCP sockets**: use it if the server must be reachable from anywhere;
* **WebSockets**: use it if the server must be reachable from anywhere, and clients don't have access to TCP stack (for example, web browsers). The file '**lib/lib\_eac.js**' contains a javascript function that can directly be used in a browser to connect to an API through WebSockets.

## Full reference

```
var eac = require('eac');
```

### Server

#### Creating and starting a server

```js
var server = eac.CreateAPI(api, eventList, protocol, fileOrPort, [callback], [debugMode]);
```

* **api**: array of functions OR object contaning some functions. These are the functions that will be "linked" to the clients;
* **eventList**: array of string representing functions that the server can call from the client;
* **protocol**: 'unix', 'tcp' or 'ws';
* **fileOrPort**: file (in case of unix sockets) or port (tcp or ws) on which the server must listen;
* **callback**: optional. Function that will be called once the server is started;
* **debugMode**: optional. If true, information sent and received is displayed (default: false).

#### Stopping the server

```js
server.Stop([callback]);
```

* **callback**: optional. Function that will be called once the server is stopped.

#### Calling clients' functions (firing events)

```js
server.clients[id].SayHello();
server.clients.SayHello();
```

* The first line calls the function "SayHello" of a specific client (**id**: integer indentifying the client).
* The second line calls the function "SayHello" of every client.

### Client

#### Connecting to a server

```js
var api = eac.ConnectAPI(address, eventMap, [callback], [debugMode]);
```

* **address**: address on which it must connect. The format depends on the protocol:
    - ws: 'ws://host:port' (ex: 'ws://my-website.com:9876');
    - tcp: 'host:port' (ex: 'localhost:4242');
    - unix: 'socketPath' (ex: '/tmp/mySocket.sock').
* **eventMap**: object associating event names with their function. If an event isn't specified, nothing will be done when it is called;
* **callback**: optional. Function that will be called once the conection has been established;
* **debugMode**: optional. If true, information sent and received is displayed (default: false).

#### Disconnecting from the server

```js
api.Disconnect([callback]);
```

* **callback**: optional. Function that will be called once it disconnected from the server.

#### Calling the server's functions

```js
var result = api.Add(18, 24, function (result) {
    console.log("18 + 24 = " + result);
});
```

* Calls the "Add" function of the server, and displays the result.
* If the last argument is a function, it will be treated as the callback (will be called after the the client receives a response from the server).

## Example: a simple chat using WebSockets

You can find the following example in the '**example**' folder. There is also an short example of how to connect to an API inside a browser.

### Server example

```js
var eac = require('eac');

var server;
var clients = [];

/* Define what events can be sent to clients. */
var chatEvents = [ "MsgReceived" ];

/*
    Define the API, i.e. what function will be provided to the clients.
    'OnConnect' and 'OnClose' are called automatically when a clients connects or disconnects.
    Inside the functions:
        'this.id' is the calling client's id.
        'this.return' if a function that send back a response to the client.
*/
var chatApi = {
    OnConnect: function (id) {
        console.log("Client " + id + " connected!");
        clients.push(id);
    },
    OnClose: function (id) {
        console.log("Client " + id + " disconnected.");
        clients.splice(clients.indexOf(id), 1);
    },
    GetClientList: function () {
        this.return(clients);
    },
    PrivateMsg: function (destId, msg) {
        var c = server.clients[destId];
        if (c === undefined) {
            this.return("InvalidId");
        } else {
            c.MsgReceived(this.id, msg);
            this.return("MessageSent");
        }
    },
    SayToEverybody: function (msg) {
        server.clients.MsgReceived(this.id, msg);
        this.return("MessageSent");
    }
};

/* Then we create the server. */
server = eac.CreateAPI(chatApi, chatEvents, 'ws', 6789, function () {
    console.log("Server ready!");
});
```

### Client example

```js
var eac = require('eac');

/* Associate each event with a function. */
var chatEventMap = {
    /* When we receive a message, we display it and then disconnect. */
    "MsgReceived": function (id, msg) {
        console.log("Message from " + id + ": " + msg);
        console.log("We will now disconnect...");
        chatApi.Disconnect();
    }
};

/* Connect to the chat and say "Hello" to everybody. */
var chatApi = eac.ConnectAPI('ws://localhost:6789', chatEventMap, function () {
    console.log("Connected!");

    chatApi.SayToEverybody("Hello!", function (response) {
        console.log("Server response: " + response);
    });
});
```