/*
    Provide a WebSocket client usable by any updated web browser
    to connect to a remote API created by EAC.
*/

function ConnectAPI(address, eventMap, callback, debugMode) {
    /* Set the debug variable. */
    var debug = debugMode === true ? true : false;

    var socket = new WebSocket(address);

    var events = SetupEvents(eventMap);

    var client = new EacClient(socket, events, debug);

    socket.onmessage = function (bigBuffer) {
        bigBuffer = bigBuffer.data;
        var subBuffers = bigBuffer.toString().split("\n");
        subBuffers.forEach(function(buffer) {
            if (buffer == "") return;
            if (client.__vars.debug) console.log("RCV: " + buffer);
            var obj = JSON.parse(buffer);

            if (!client.__vars.ready) {
                SetupAPI(client, obj);
                client.__vars.ready = true;
                if (callback) callback();
            } else if (obj.evt !== undefined) {
                var f = client.__vars.events[obj.evt];
                if (f !== undefined) {
                    f.apply(f, obj.args);
                }
            } else {
                client.__vars.callbacks[obj.id](obj.data);
                delete client.__vars.callbacks[obj.id];
            }
        });
    };

    return client;
}

function EacClient(socket, events, debug) {
    this.__vars = new Object();
    this.__vars.socket = socket;
    this.__vars.nextCallId = 1;
    this.__vars.callbacks = new Object();
    this.__vars.events = events;
    this.__vars.ready = false;
    this.__vars.debug = debug;
}

EacClient.prototype.Disconnect = function () {
    this.__vars.socket.close();
}


function SetupAPI(client, functionList) {
    functionList.forEach(function (name) {
        client[name] = function () {
            var lastIndex = Object.keys(arguments).length - 1;
            var lastArg = arguments[lastIndex];
            var callId, packet;
            if (typeof lastArg === "function") { // This is the callback
                client.__vars.callbacks[client.__vars.nextCallId] = lastArg;
                delete arguments[lastIndex];
                callId = client.__vars.nextCallId;
                client.__vars.nextCallId += 1;
            } else {                             // No callback was specified
                callId = null;
            }
            var args = new Array();
            for (var k in arguments) args.push(arguments[k]);

            var packet = JSON.stringify({id:callId, name:name, args:args});
            if (client.__vars.debug) console.log("SNT: " + packet);
            client.__vars.socket.send(packet + "\n");
        };
    });
}

function SetupEvents(eventMap) {
    var events = new Object();
    if (eventMap instanceof Array) {
        eventMap.forEach(function (f) {
            if (typeof f === "function") {
                events[f.name] = f;
            }
        });
    } else if (eventMap instanceof Object) {
        for (var n in eventMap) {
            var f = eventMap[n];
            if (typeof f === "function") {
                events[n] = f;
            }
        }
    }

    return events;
}
