var eac = require('eac');

/* Associate each event with a function. */
var chatEventMap = {
    /* When we receive a message, we display it and then disconnect. */
    "MsgReceived": function (id, msg) {
        console.log("Message from " + id + ": " + msg);
        console.log("We will now disconnect...");
        chatApi.Disconnect();
    }
};

/* Connect to the chat and say "Hello" to everybody. */
var chatApi = eac.ConnectAPI('ws://localhost:6789', chatEventMap, function () {
    console.log("Connected!");

    chatApi.SayToEverybody("Hello!", function (response) {
        console.log("Server response: " + response);
    });
});
