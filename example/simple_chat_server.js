var eac = require('eac');

var server;
var clients = [];

/* Define what events can be sent to clients. */
var chatEvents = [ "MsgReceived" ];

/*
    Define the API, i.e. what function will be provided to the clients.
    'OnConnect' and 'OnClose' are called automatically when a clients connects or disconnects.
    Inside the functions:
        'this.id' is the calling client's id.
        'this.return' if a function that send back a response to the client.
*/
var chatApi = {
    OnConnect: function (id) {
        console.log("Client " + id + " connected!");
        clients.push(id);
    },
    OnClose: function (id) {
        console.log("Client " + id + " disconnected.");
        clients.splice(clients.indexOf(id), 1);
    },
    GetClientList: function () {
        this.return(clients);
    },
    PrivateMsg: function (destId, msg) {
        var c = server.clients[destid];
        if (c === undefined) {
            this.return("InvalidId");
        } else {
            c.MsgReceived(this.id, msg);
            this.return("MessageSent");
        }
    },
    SayToEverybody: function (msg) {
        server.clients.MsgReceived(this.id, msg);
        this.return("MessageSent");
    }
};

/* Then we create the server. */
server = eac.CreateAPI(chatApi, chatEvents, 'ws', 6789, function () {
    console.log("Server ready!");
});
